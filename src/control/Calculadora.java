package control;


import java.math.BigDecimal;


public class Calculadora {

	private StringBuilder operandoSB, historial;
	private double resultado;
	private String operacion;
	private double[] memoria;
	private boolean modoMemRecall, modoMemStorage, simboloPermitido, muestraRes;
	
	
	public Calculadora() {
		inicializar();
		this.memoria = new double[10];
		for (int x=0;x<memoria.length;x++) {
			memoria[x] = 0.0;
		}
	}
	
	public void inicializar() {
		this.operandoSB = new StringBuilder();
		this.historial = new StringBuilder();
	    this.operacion = "";
		this.resultado = 0.0;
		this.modoMemRecall = false;
		this.modoMemStorage = false;
		this.simboloPermitido = false;
		this.muestraRes = false;
	}

	
	//************************	
	//* FUNCIONES DE MEMORIA *
	//************************
	public String getMem(int pos) {
		BigDecimal numero = new BigDecimal(Double.toString(memoria[pos]));  
		String num = numero.stripTrailingZeros().toPlainString();
		historial.append(num+" + ");
		return num;
	}
	
	public double setMem(int pos, double n) {
		return memoria[pos] = n;
	}
	
	public void clearMem() {
		for (int x=0;x<memoria.length;x++) {
			memoria[x] = 0.0;
		}
	}
	
	public boolean memoriaEstaVacia() {
		boolean ret = true;
		for (int x=0;x<memoria.length;x++) {
			if (memoria[x] != 0.0) {
				ret = false;
			}
		}
		return ret;
	}
		
	public String getMemoriaStr() {
		String mem = "VALORES EN MEMORIA:\n\n";
		int cont = 0;
		for (double m: getMemoria() ) {
			String aux = "M"+Integer.toString(cont)+": "+Double.toString(m)+"\n";
			mem = mem + aux;
			cont++;
		}
		return mem;
	}

	
	//********************************************
	//* FUNCIONES DE INGRESO Y MODIF. DE VALORES *
	//********************************************
	public void recibeValorStr(String valorIngresado) {
		if (Funciones.esNumero(valorIngresado)) {
			operandoSB.append(valorIngresado);
			simboloPermitido = true;
			muestraRes = false;
			resolver();
		} else if (valorIngresado.equals(".")) {
			agregarPuntoDecimalAOperando();
			simboloPermitido = true;
			muestraRes = false;
			resolver();
		} else if (Funciones.esOperacion(valorIngresado)) {
			//SI ES NUN NEG
			if (valorIngresado.equals("-") && operandoSB.toString().equals("")) {
				operandoSB.append("-");
			} else { 
				//SI EL ULTIMO CARACTER ES OP. LA REEMPLAZA
				agregaOpAlHistorial(valorIngresado);
				operandoSB = new StringBuilder("0");
				muestraRes = true;
				resolver();
				
				String[] arr= historial.toString().split(" ");
				if (arr.length<=2) {
					try {
						resultado = Double.parseDouble(arr[0]);	
					} catch (Exception e) {
						resultado = 0;
					}
				}
			}
		}
	}
	
	public void agregarPuntoDecimalAOperando() {
		int lastI = operandoSB.length()-1;
		
		if (operandoSB.charAt(lastI) != '.' && operandoSB.length() >= 1) {
			operandoSB.append(".");	
		}
	}
	
	public void eliminarUltimoNumeroOp() {
		if (operandoSB.length()>0) {
			operandoSB.deleteCharAt(operandoSB.length()-1);
			Funciones.eliminaCeroALaIzq(operandoSB);
		}
	}
	
	
	//**************************	
	//* FUNCIONES DE HISTORIAL *
	//**************************	
	public void agregaOpAlHistorial(String v) {
		if (simboloPermitido) {
			historial.append(Funciones.eliminaCeroALaIzq(operandoSB).toString()+" "+v+" ");	
			simboloPermitido = false;
				
		} else {
			if (historial.length()>0) {
				int lastI = historial.length()-1;
				historial.replace(lastI-1, lastI, v);
			}
		}
	}

	public void deshacer() {
		//SEPARA N�MEROS Y OP. EN ARRAY DE STRINGS
		String[] arr= historial.toString().split(" ");
			
		if (arr.length>2) {
			//VAC�A HISTORIAL
			historial = new StringBuilder("");
			
			//AGREGA AL HISTORIAL TODO MENOS �LTIMO N�M Y OP.
			for (int i = 0; i<arr.length-2; i++) {
				historial.append(arr[i]+" ");
			}
		}
		resolver();
	}

	
	//***************************	
	//* FUNCIONES DE RESOLUCI�N *
	//***************************
	public void resolver() {
		//SEPARA N�MEROS Y OP. EN ARRAY DE STRINGS
		String[] arr= historial.toString().split(" ");

		if (arr.length>2) {

			double calc = Double.parseDouble(arr[0]);
			
			for (int i = 2; i<arr.length-1; i+=2) {
				
				switch (arr[i-1]) {
				case "+":
					calc = calc + Double.parseDouble(arr[i]);
					break;
				case "-":
					calc = calc - Double.parseDouble(arr[i]);
					break;
				case "/":
					calc = calc / Double.parseDouble(arr[i]);
					break;
				case "*":
					calc = calc * Double.parseDouble(arr[i]);
					break;
				}
				
			}
			resultado = calc;
		} else {
			if (operandoSB.length()>0) { 
				resultado = Double.parseDouble(operandoSB.toString());	
			}
		}
	}
	
	//*********************	
	//* GETTERS Y SETTERS *
	//*********************
	public StringBuilder getOperandoSB() {
		return operandoSB;
	}

	public void setOperandoSB(StringBuilder op) {
		this.operandoSB = op;
	}
	
	public StringBuilder getHistorial() {
		return historial;
	}

	public void setHistorial(StringBuilder historial) {
		this.historial = historial;
	}

	public double getResultado() {
		return resultado;
	}

	public void setResultado(double resultado) {
		this.resultado = resultado;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	
	public double[] getMemoria() {
		return memoria;
	}

	public void setMemoria(double[] memoria) {
		this.memoria = memoria;
	}

	public boolean isModoMemRecall() {
		return modoMemRecall;
	}

	public void setModoMemRecall(boolean modoMemRecall) {
		this.modoMemRecall = modoMemRecall;
	}

	public boolean isModoMemStorage() {
		return modoMemStorage;
	}

	public void setModoMemStorage(boolean modoMemStorage) {
		this.modoMemStorage = modoMemStorage;
	}

	public boolean isSimboloPermitido() {
		return simboloPermitido;
	}

	public void setSimboloPermitido(boolean ult) {
		this.simboloPermitido = ult;
	}

	public boolean isMuestraRes() {
		return muestraRes;
	}

	public void setMuestraRes(boolean muestraRes) {
		this.muestraRes = muestraRes;
	}
	
}
