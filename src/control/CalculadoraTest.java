package control;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class CalculadoraTest {
	Calculadora c;
	
	@Before
	public void inicializar(){
		c = new Calculadora();
	}
	
	//*******************	
	//* TEST DE INGRESO *
	//*******************
	@Test
	public void recibeValorStrNumeroTest() {	
		c.recibeValorStr("1");
		c.recibeValorStr("2");
		
		assertEquals("12", c.getOperandoSB().toString());
	}
	
	@Test
	public void recibeValorStrDecimalTest() {	
		c.recibeValorStr("1");
		c.recibeValorStr(".");
		c.recibeValorStr("0");
		c.recibeValorStr("2");
		
		assertEquals("1.02", c.getOperandoSB().toString());
	}
	
	@Test
	public void recibeValorStrNegativoTest() {	
		c.recibeValorStr("-");
		c.recibeValorStr("1");
		
		assertEquals("-1", c.getOperandoSB().toString());
	}
	
	//****************************
	//* FUNCIONES DE OPERACIONES *
	//****************************
	@Test
	public void resuelveSumaTest() {	
		c.recibeValorStr("2");
		c.recibeValorStr("+");
		c.recibeValorStr("4");
		
		c.recibeValorStr("=");
		assertEquals(6, c.getResultado(), 0.001);
	}
	
	@Test
	public void resuelveRestaTest() {	
		c.recibeValorStr("2");
		c.recibeValorStr("-");
		c.recibeValorStr("4");
		c.recibeValorStr("=");
		assertEquals(-2, c.getResultado(), 0.001);
	}
	
	@Test
	public void resuelveDivisionTest() {	
		c.recibeValorStr("8");
		c.recibeValorStr("/");
		c.recibeValorStr("2");
		c.recibeValorStr("=");
		assertEquals(4, c.getResultado(), 0.001);
	}
	
	@Test
	public void resuelveMultiplicacionTest() {	
		c.recibeValorStr("8");
		c.recibeValorStr("*");
		c.recibeValorStr("2");
		c.recibeValorStr("=");
		assertEquals(16, c.getResultado(), 0.001);
	}
	
	//*****************	
	//* TEST DESHACER *
	//*****************
	@Test
	public void deshacerTest() {	
		c.recibeValorStr("2");
		c.recibeValorStr("+");
		c.recibeValorStr("4");
		c.recibeValorStr("+");
		c.recibeValorStr("4");
		c.recibeValorStr("=");
		
		c.deshacer();
		assertEquals(6, c.getResultado(), 0.001);
	}
	
	//*******************	
	//* TEST DE MEMORIA *
	//*******************
	@Test
	public void setYGetMemTest() {	
		c.setMem(0, 1.1);
		
		assertEquals(1.1, c.getMemoria()[0], 0.001);
	}
	
	@Test
	public void clearMemTest() {	
		//LLENA LA MEMORIA CON 1
		for(int i=0; i<c.getMemoria().length; i++) {
			c.getMemoria()[i] = 1.0;
		}
		c.clearMem();
		
		assertTrue(c.memoriaEstaVacia());
	}
	
	//*********************	
	//* TEST DE HISTORIAL *
	//*********************
	public void agregarOpAlHistorialTest() {	
		c.recibeValorStr("1");
		c.agregaOpAlHistorial("+");
		
		assertEquals("1 + ", c.getHistorial().toString());
	}
}
