package control;

public class Funciones {
	
	//************************	
	//* FUNCIONES AUXILIARES *
	//************************
	public static boolean esNumero(String s) {
        boolean resultado;

        try {
            Integer.parseInt(s);
            resultado = true;
        } catch (NumberFormatException e) {
            resultado = false;
        }

        return resultado;
    }
	
	
	public static boolean esOperacion(String s) {
		String[] ops = {"+","-","/","*","="};
        
		for (String op: ops) {
			if (s.equals(op)) {
				return true;
			}
		}
		
        return false;
    }
	
	public static StringBuilder eliminaCeroALaIzq(StringBuilder sb) {
		
		if (sb.charAt(0) == '0') {
			sb.deleteCharAt(0);
		}
		
		return sb;
	}
}
