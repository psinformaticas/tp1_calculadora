package form;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JScrollPane;

import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.awt.event.ActionEvent;

import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import control.Calculadora;

import java.awt.Color;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

import java.awt.Font;

import javax.swing.JOptionPane;


public class Ventana {

	private JFrame frmCalculadora;
	private Calculadora c;
	private JLabel linea1;
	private JLabel linea2;
	private JLabel memLb;
	private JPanel visor;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventana window = new Ventana();
					window.frmCalculadora.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ventana() {
		initialize();
		c = new Calculadora(); 

	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		frmCalculadora = new JFrame();
		frmCalculadora.setTitle("Calculadora");
		frmCalculadora.setResizable(false);
		frmCalculadora.getContentPane().setBackground(Color.BLACK);
		frmCalculadora.setBounds(100, 100, 281, 562);
		frmCalculadora.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmCalculadora.getContentPane().setLayout(null);
		frmCalculadora.setLocationRelativeTo(null);

		
		crearBotonesNum();
		
		
		JButton btnDec = new JButton(",");
		btnDec.setBorder(new LineBorder(new Color(0, 0, 0)));
		btnDec.setBackground(Color.WHITE);
		btnDec.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				accionBoton(".");
			}
		});
		btnDec.setBounds(138, 457, 70, 70);
		frmCalculadora.getContentPane().add(btnDec);
		
		JButton btnPower = new JButton("");
		btnPower.setBorder(new LineBorder(new Color(0, 0, 0)));
		btnPower.setBackground(Color.WHITE);
		btnPower.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			
			}
		});
		btnPower.setForeground(new Color(0, 128, 0));
		btnPower.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnPower.setBounds(0, 457, 70, 70);
		frmCalculadora.getContentPane().add(btnPower);
		
		JButton btnCe = new JButton("CE");
		btnCe.setFont(new Font("Tahoma", Font.BOLD, 24));
		btnCe.setBackground(new Color(30, 144, 255));
		btnCe.setBorder(new LineBorder(new Color(0, 0, 0)));
		btnCe.setForeground(new Color(255, 255, 255));
		btnCe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				c.setOperandoSB(new StringBuilder());
				c.setResultado(0);
				linea1.setText("0");
			}
		});
		btnCe.setBounds(0, 181, 70, 70);
		frmCalculadora.getContentPane().add(btnCe);
		
		JButton btnC = new JButton("C");
		btnC.setFont(new Font("Tahoma", Font.BOLD, 24));
		btnC.setBackground(new Color(30, 144, 255));
		btnC.setBorder(new LineBorder(new Color(0, 0, 0)));
		btnC.setForeground(new Color(255, 255, 255));
		btnC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				limpiarCalc();
			}
		});
		btnC.setBounds(69, 181, 70, 70);
		frmCalculadora.getContentPane().add(btnC);
		
		JButton btnBorrar = new JButton("<x");
		btnBorrar.setFont(new Font("Tahoma", Font.BOLD, 24));
		btnBorrar.setBackground(new Color(30, 144, 255));
		btnBorrar.setBorder(new LineBorder(new Color(0, 0, 0)));
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				c.eliminarUltimoNumeroOp();
				linea1.setText((c.getOperandoSB()).toString());
			}
		});
		
		btnBorrar.setForeground(new Color(255, 255, 255));
		btnBorrar.setToolTipText("<x");
		btnBorrar.setBounds(138, 181, 70, 70);
		frmCalculadora.getContentPane().add(btnBorrar);
		
		JButton btnDiv = new JButton("/");
		btnDiv.setFont(new Font("Tahoma", Font.BOLD, 24));
		btnDiv.setBackground(new Color(255, 165, 0));
		btnDiv.setBorder(new LineBorder(new Color(0, 0, 0)));
		btnDiv.setForeground(Color.WHITE);
		btnDiv.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				accionBoton("/");
			}
		});
		btnDiv.setToolTipText("/");
		btnDiv.setBounds(207, 181, 70, 70);
		frmCalculadora.getContentPane().add(btnDiv);
		
		JButton btnMult = new JButton("X");
		btnMult.setFont(new Font("Tahoma", Font.BOLD, 24));
		btnMult.setBackground(new Color(255, 165, 0));
		btnMult.setBorder(new LineBorder(new Color(0, 0, 0)));
		btnMult.setForeground(Color.WHITE);
		btnMult.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				accionBoton("*");
			}
		});
		btnMult.setBounds(207, 250, 70, 70);
		frmCalculadora.getContentPane().add(btnMult);
		
		JButton btnResta = new JButton("-");
		btnResta.setFont(new Font("Tahoma", Font.BOLD, 24));
		btnResta.setBackground(new Color(255, 165, 0));
		btnResta.setBorder(new LineBorder(new Color(0, 0, 0)));
		btnResta.setForeground(Color.WHITE);
		btnResta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				accionBoton("-");
			}
		});
		btnResta.setBounds(207, 388, 70, 70);
		frmCalculadora.getContentPane().add(btnResta);
		
		JButton btnSuma = new JButton("+");
		btnSuma.setFont(new Font("Tahoma", Font.BOLD, 24));
		btnSuma.setBackground(new Color(255, 165, 0));
		btnSuma.setBorder(new LineBorder(new Color(0, 0, 0)));
		btnSuma.setForeground(Color.WHITE);
		btnSuma.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				accionBoton("+");
			}
		});
		btnSuma.setBounds(207, 319, 70, 70);
		frmCalculadora.getContentPane().add(btnSuma);
		
		JButton btnIgual = new JButton("=");
		btnIgual.setFont(new Font("Tahoma", Font.BOLD, 24));
		btnIgual.setBorder(new LineBorder(new Color(0, 0, 0)));
		btnIgual.setBackground(new Color(255, 165, 0));
		btnIgual.setForeground(Color.WHITE);
		btnIgual.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				accionBoton("=");
				setVisor();
			}
		});
		btnIgual.setBounds(207, 457, 70, 70);
		frmCalculadora.getContentPane().add(btnIgual);
		
		visor = new JPanel();
		visor.setBackground(new Color(255, 255, 255));
		visor.setBorder(new LineBorder(new Color(0, 0, 0)));
		visor.setBounds(0, 0, 277, 94);
		frmCalculadora.getContentPane().add(visor);
		visor.setLayout(null);
		
		linea2 = new JLabel("0");
		linea2.setForeground(Color.BLACK);
		linea2.setFont(new Font("OCR A Extended", Font.PLAIN, 13));
		linea2.setHorizontalAlignment(SwingConstants.TRAILING);
		linea2.setBounds(29, 13, 236, 16);
		visor.add(linea2);
		
		memLb = new JLabel("");
		memLb.setHorizontalAlignment(SwingConstants.TRAILING);
		memLb.setForeground(Color.BLACK);
		memLb.setFont(new Font("OCR A Extended", Font.BOLD, 20));
		memLb.setBounds(0, 31, 26, 33);
		visor.add(memLb);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBorder(null);
		scrollPane.getViewport().setBackground(Color.WHITE);
		scrollPane.setForeground(Color.WHITE);
		scrollPane.setBackground(Color.WHITE);
		scrollPane.setBounds(29, 31, 239, 51);
		visor.add(scrollPane);
		
		linea1 = new JLabel("0");
		linea1.setBackground(Color.WHITE);
		scrollPane.setViewportView(linea1);
		linea1.setForeground(Color.BLACK);
		linea1.setFont(new Font("OCR A Extended", Font.BOLD, 20));
		linea1.setHorizontalAlignment(SwingConstants.TRAILING);
		
		
		//BOTONES MEMORIA
		JButton btnMS = new JButton("MS");
		btnMS.setFont(new Font("Tahoma", Font.BOLD, 24));
		btnMS.setBackground(new Color(0, 128, 0));
		btnMS.setBorder(new LineBorder(new Color(0, 0, 0)));
		btnMS.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				c.setModoMemStorage(true);
			}
		});
		btnMS.setForeground(new Color(255, 255, 255));
		btnMS.setBounds(0, 132, 70, 50);
		frmCalculadora.getContentPane().add(btnMS);
		
		JButton btnMR = new JButton("MR");
		btnMR.setFont(new Font("Tahoma", Font.BOLD, 24));
		btnMR.setBackground(new Color(0, 128, 0));
		btnMR.setBorder(new LineBorder(new Color(0, 0, 0)));
		btnMR.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				c.setModoMemRecall(true);
			}
		});
		btnMR.setForeground(new Color(255, 255, 255));
		btnMR.setBounds(69, 132, 70, 50);
		frmCalculadora.getContentPane().add(btnMR);
		
		JButton btnMC = new JButton("MC");
		btnMC.setFont(new Font("Tahoma", Font.BOLD, 24));
		btnMC.setBackground(new Color(0, 128, 0));
		btnMC.setBorder(new LineBorder(new Color(0, 0, 0)));
		btnMC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				c.clearMem();
				setVisorMemoria();
			}
		});
		btnMC.setForeground(new Color(255, 255, 255));
		btnMC.setBounds(138, 132, 70, 50);
		frmCalculadora.getContentPane().add(btnMC);
		
		JButton btnCancelMem = new JButton("X");
		btnCancelMem.setFont(new Font("Tahoma", Font.BOLD, 24));
		btnCancelMem.setBackground(new Color(0, 128, 0));
		btnCancelMem.setBorder(new LineBorder(new Color(0, 0, 0)));
		btnCancelMem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				c.setModoMemRecall(false);
				c.setModoMemStorage(false);
			}
		});
		btnCancelMem.setForeground(new Color(255, 255, 255));
		btnCancelMem.setBounds(207, 132, 70, 50);
		frmCalculadora.getContentPane().add(btnCancelMem);
		
		JButton btnDeshacer = new JButton("Deshacer");
		btnDeshacer.setForeground(new Color(255, 255, 255));
		btnDeshacer.setFont(new Font("Tahoma", Font.BOLD, 19));
		btnDeshacer.setBackground(new Color(192, 192, 192));
		btnDeshacer.setBorder(new LineBorder(new Color(0, 0, 0)));
		btnDeshacer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				c.deshacer();
				accionBoton("=");
				setVisor();
			}
		});
		btnDeshacer.setBounds(138, 93, 139, 40);
		frmCalculadora.getContentPane().add(btnDeshacer);
		
		JButton btnVerMem = new JButton("Ver MEM");
		btnVerMem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!c.memoriaEstaVacia()) {
					JOptionPane.showMessageDialog(null, c.getMemoriaStr(), "MEMORIA", 1);
				}
			}
		});
		btnVerMem.setForeground(Color.WHITE);
		btnVerMem.setFont(new Font("Tahoma", Font.BOLD, 19));
		btnVerMem.setBorder(new LineBorder(new Color(0, 0, 0)));
		btnVerMem.setBackground(Color.LIGHT_GRAY);
		btnVerMem.setBounds(0, 93, 139, 40);
		frmCalculadora.getContentPane().add(btnVerMem);
	}
	
	//************************	
	//* FUNCIONES DE BOTONES *
	//************************
	
	private void crearBotonesNum() {
		int cantBotones = 10;
		
		for (int num=0; num<cantBotones; num++) {
			JButton newButton = new JButton(Integer.toString(num));
			newButton.setBorder(new LineBorder(new Color(0, 0, 0)));
			newButton.setFocusPainted(false);
			newButton.setFont(new Font("Tahoma", Font.BOLD, 21));
			newButton.setBackground(Color.WHITE);
			newButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				accionBoton(newButton.getText());
				}
			});
						

			//ASIGNA POSICIONES
			switch (num) {
				case 0: newButton.setBounds(69, 457, 70, 70);
				break;
				case 1: newButton.setBounds(0, 388, 70, 70);
				break;
				case 2: newButton.setBounds(69, 388, 70, 70);
				break;
				case 3: newButton.setBounds(138, 388, 70, 70);
				break;
				case 4: newButton.setBounds(0, 319, 70, 70);
				break;
				case 5: newButton.setBounds(69, 319, 70, 70);
				break;
				case 6: newButton.setBounds(138, 319, 70, 70);
				break;
				case 7: newButton.setBounds(0, 250, 70, 70);
				break;
				case 8: newButton.setBounds(69, 250, 70, 70);
				break;
				case 9: newButton.setBounds(138, 250, 70, 70);
				break;

			}
			frmCalculadora.getContentPane().add(newButton);
		}

	}
	
	private void accionBoton(String boton) {
		if (c.isModoMemStorage()) {
			c.setMem(Integer.parseInt(boton), Double.parseDouble(linea1.getText()));
			c.setModoMemStorage(false);
			setVisorMemoria();
		} else if (c.isModoMemRecall()) {
			c.setOperandoSB(new StringBuilder(c.getMem(Integer.parseInt(boton))));
			c.setModoMemRecall(false);
			setVisor();
		} else {
			c.recibeValorStr(boton);
			setVisor();
		}	
	}
	
	
	//**********************	
	//* FUNCIONES DE VISOR *
	//**********************
	private void limpiarCalc() {
		linea1.setText("0");
		linea2.setText("0");
		c.inicializar();
	}
	
	private void setVisor() {
		BigDecimal res;
		
		try {
			if (c.isMuestraRes() && !c.isModoMemRecall()) {
				res = new BigDecimal(Double.toString(c.getResultado()));	
			} else {
				if (c.getOperandoSB().toString().equals("-")) {
					res = new BigDecimal("0.0");
				} else {
					if (c.isMuestraRes()){
						res = new BigDecimal(Double.toString(c.getResultado()));
					} else {
						res = new BigDecimal(c.getOperandoSB().toString());
					}
						
				}
				
			}
			
			linea1.setText(res.setScale(3, BigDecimal.ROUND_DOWN).stripTrailingZeros().toPlainString());
			linea2.setText(c.getHistorial().toString());
			setVisorMemoria();
			
		} catch (Exception e) {
			setErrorEnVisor(e.toString().replaceAll("java.lang.", ""));
		}
	}
	
	private void setVisorMemoria() {
		if (c.memoriaEstaVacia()) {
			memLb.setText("");
		} else {
			memLb.setText("M");
		}
	}
	
	private void setErrorEnVisor(String e) {
		linea1.setText("ERROR");
		linea2.setText(e);
	}
}
